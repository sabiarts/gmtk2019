﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ActionButton : MonoBehaviour
{
    Player player;
    Vector3 touchPosition;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void Update(){
        for (int currentTouch = 0 ; currentTouch < Input.touchCount; currentTouch ++){
            touchPosition = Camera.main.ScreenToWorldPoint(Input.touches[currentTouch].position); // touchPosition = Input.touches[currentTouch].position;
            if (Vector2.Distance(touchPosition, transform.position) <= 1.8f){
                player.Faz_dash_botao();
            }
        }
    }
}

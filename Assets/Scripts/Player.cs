﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool advancedJump;
    public float moveSpeed;
    public float jumpForce;
    public float fallMultiplier;
    public float lowJumpMultiplier;
    public int dashDuration;

    public Transform darkness;

    public AudioClip seDash;
    public AudioClip seJump;
    public AudioClip seWalk;
    public AudioClip seDeath;
    public AudioClip seCrystal1;
    public AudioClip seCrystal2;
    public AudioClip seCrystal3;

    int jumpCooldown;
    int direction = 1;
    int lockedDirection = 0;
    bool isMoving;

    GameControl gameControl;
    AudioSource soundPlayer;
    Rigidbody2D playerBody;
    float defaultGravity;
    int activeSkill;
    bool onGround;
    bool airAction;
    bool dashing = false;
    bool dead = false;


    void Start()
    {
        gameControl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameControl>(); 
        soundPlayer = gameObject.GetComponent<AudioSource>();
        playerBody = gameObject.GetComponent<Rigidbody2D>();
        defaultGravity = playerBody.gravityScale;
        activeSkill = 0;
    }

    void FixedUpdate()
    {
        PlayerMove();
        if (playerBody.velocity.y < -0.6){

            Vector2 fallApply = Vector2.up * Physics2D.gravity.y * (fallMultiplier - defaultGravity) * Time.deltaTime;
            if (fallApply.x < 4 && fallApply.x > -4 && fallApply.y < 4 && fallApply.y > -4){
                playerBody.velocity += fallApply;


            }
        } /*else if ((advancedJump) && playerBody.velocity.y > 0 && !Input.GetButton("Jump")){
            playerBody.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - defaultGravity) * Time.deltaTime; 
        }*/

        if (activeSkill != 0){
            darkness.position = GetComponent<Transform>().position;
        }
        if (playerBody.velocity.x > 11 || playerBody.velocity.y > 11 ){
            playerBody.velocity = new Vector2(0,0);
        }

    }

    void PlayerMove(){
        if (!dashing && !dead){
            if (onGround && Input.GetAxisRaw("Horizontal") != 0 && !soundPlayer.isPlaying){
                soundPlayer.clip = seWalk;
                soundPlayer.Play();
            }

            Vector2 speedApply = new Vector2(moveSpeed * Time.fixedDeltaTime * Input.GetAxisRaw("Horizontal"),
                                                playerBody.velocity.y);
            
            if((lockedDirection > 0 && speedApply.x > 0) || (lockedDirection < 0 && speedApply.x < 0)){
                speedApply.x = 0;
                Debug.Log("Zerou");
            }

            if (speedApply.x < 20 && speedApply.x > -20 && speedApply.y < 20 && speedApply.y > -20){
                playerBody.velocity = speedApply;
            }
                
            if (Input.GetAxisRaw("Horizontal") > 0){
                
                direction = 1;
                //Debug.Log("-> " + Input.GetAxisRaw("Horizontal").ToString());
                setIsMoving(true);

            }
            if (Input.GetAxisRaw("Horizontal") < 0){
                
                direction = -1;
                //Debug.Log("<- " + Input.GetAxisRaw("Horizontal").ToString());
                setIsMoving(true);

            }
            if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0){
                //Debug.Log("x");
                setIsMoving(false);
            }
            if ((Input.GetButtonDown("Jump") || Input.GetAxisRaw("Vertical") > 0.5f) && jumpCooldown == 0){
                if(onGround){
                    Jump();
                /*}else if(activeSkill == 2 && !onGround && !airAction){
                    airAction = true;
                    playerBody.velocity = new Vector2(0,0);
                    Jump();*/
                } 
                jumpCooldown = 15;
            }
            if (Input.GetButtonDown("Fire1")) {
                if (activeSkill == 1){
                    if(onGround || !airAction) {
                        if (!onGround){
                            airAction = true;
                        }
                        playerBody.velocity = new Vector2(0,0);
                        StartCoroutine(Dash());
                    } 
                } else if(activeSkill == 2 && !onGround && !airAction){
                    airAction = true;
                    playerBody.velocity = new Vector2(0,0);
                    Jump();
                }
            }
        }
        if (jumpCooldown > 0){
            jumpCooldown --;
        }
        SpriteWork();
    }

    public bool setIsMoving(bool mov){
        isMoving = mov;
        gameObject.GetComponent<Animator>().SetBool("isMoving", mov);
        return isMoving;
    }

    void Jump(){
        playerBody.velocity += jumpForce * Vector2.up;
        soundPlayer.clip = seJump;
        soundPlayer.Play();
    }

    IEnumerator Dash(){
        soundPlayer.clip = seDash;
        soundPlayer.Play();
        dashing = true;
        gameObject.GetComponent<Animator>().SetBool("dashing", dashing);
        playerBody.gravityScale = 0;
        playerBody.position = new Vector2(playerBody.position.x, playerBody.position.y + 0.08f);
        for(int dashTime = 0; dashTime < dashDuration; dashTime++){
            yield return new WaitForFixedUpdate();
            playerBody.velocity = new Vector2((moveSpeed * 3) * Time.fixedDeltaTime * direction ,0);
        }
        playerBody.gravityScale = defaultGravity;
        dashing = false;
        gameObject.GetComponent<Animator>().SetBool("dashing", dashing);
    }

    public void ChangeActiveSkill(int skill){
        if (activeSkill != skill){
            airAction = false;  
        }
        activeSkill = skill;    
        if (skill == 0){
            darkness.gameObject.SetActive(false);
            soundPlayer.clip = seCrystal1;
        } else {
            darkness.gameObject.SetActive(true);
            if (skill == 1){
                soundPlayer.clip = seCrystal2;
            } else {
                soundPlayer.clip = seCrystal3;
            }
        }
        soundPlayer.Play();
    }

    void OnTriggerEnter2D(Collider2D col){
        if (col.gameObject.tag == "Hazzard"){
            if (!dead){
                soundPlayer.clip = seDeath;
                soundPlayer.Play();
                StartCoroutine(GameOver());
            }
        }

        if (col.gameObject.tag == "Exit"){
            gameControl.EndLevel(col.gameObject.GetComponent<Exit>().nextLevelName);
        }
    }

    IEnumerator GameOver(){
        dead = true;
        playerBody.velocity = new Vector2(0,0);
        GetComponent<Collider2D>().enabled = false;
        playerBody.velocity += jumpForce * Vector2.up;
        yield return new WaitForSeconds(1);
        gameControl.Restart();
    }

    public void SetOnGround(bool grounded){
        if (grounded){
            onGround = true;
            airAction = false;
            //soundPlayer.Play();
        } else{
            onGround = false;
        }
        gameObject.GetComponent<Animator>().SetBool("onGround", onGround);
    }

    public void Faz_dash_botao(){
    	        if (activeSkill == 1){
                    if(onGround || !airAction) {
                        if (!onGround){
                            airAction = true;
                        }
                        playerBody.velocity = new Vector2(0,0);
                        StartCoroutine(Dash());
                    } 
                } else if(activeSkill == 2 && !onGround && !airAction){
                    airAction = true;
                    playerBody.velocity = new Vector2(0,0);
                    Jump();
                }
    }

    void SpriteWork(){
        if (direction == 1){
            if (gameObject.GetComponent<Transform>().localScale.x < 0){
                gameObject.GetComponent<Transform>().localScale *= new Vector2(-1,1);
                gameObject.GetComponent<Transform>().localScale = new Vector3(gameObject.GetComponent<Transform>().localScale.x,gameObject.GetComponent<Transform>().localScale.y,1);
            }
        }
        if (direction == -1){
            if (gameObject.GetComponent<Transform>().localScale.x > 0){
                gameObject.GetComponent<Transform>().localScale *= new Vector2(-1,1);
                gameObject.GetComponent<Transform>().localScale = new Vector3(gameObject.GetComponent<Transform>().localScale.x,gameObject.GetComponent<Transform>().localScale.y,1);
            }
        }
    }

    public void LockDirection(){
        lockedDirection = direction;
    }

    public void UnlockDirection(){
        lockedDirection = 0;
    }

    public int GetDirection(){
        return direction;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GotoScene : MonoBehaviour
{
    // Start is called before the first frame update
public void ChangeScene(string nextScene){
        SceneManager.LoadScene(nextScene);
    }

        public void StopMusic(){
        Destroy(GameObject.FindGameObjectWithTag("Jukebox"));
    }

    public void Exit (){
    	Application.Quit();
    }
}

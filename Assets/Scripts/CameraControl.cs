﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public Vector2 positiveLimit;
    public Vector2 negativeLimit;
    public bool followY;
    public bool followX;

    Transform player;
    
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void FixedUpdate()
    {
        if (followX){
            if(player.position.x < positiveLimit.x && player.position.x > negativeLimit.x){
                transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
            }
        }
        if (followY){
            if(player.position.y < positiveLimit.y && player.position.y > negativeLimit.y){
                transform.position = new Vector3(transform.position.x, player.transform.position.y, transform.position.z);
            }
        }
    }
}
